<?php
/**
 * Created by PhpStorm.
 * User: muslim
 * Date: 06.11.18
 * Time: 13:38
 */

namespace Services\User;

use Interfaces\User\UserRepositoryInterface;
use Entities\User\User;

class UserService
{
    private $userRepository;

    /**
     * UserService constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Авторизация
     * @param string $username
     * @param string $password
     * @return User
     * @throws \Exception
     */
    public function login(string $username, string $password)
    {
        /** @var User $user */
        $user = $this->userRepository->findByName($username)[0];

        if ($user === null || !$user->verifyPassword($password)) {
            throw new \Exception('Неверный логин или пароль');
        }

        $out = ob_get_contents();
        ob_end_clean();
        setcookie('id', $user->getId(), time() + 60);
        setcookie('user', md5($username), time() + 60);
        setcookie('password', md5($password), time() + 60);
        echo $out;
        $user->setLastAuthorization(date('Y-m-d H:i:s'));

        $this->userRepository->update($user);

        return $user;
    }

    /**
     * @param string $username
     * @param string $password
     * @throws \Exception
     */
    public function registration(string $username, string $password)
    {
        $this->userRepository->create($username, $password);
    }

    public function inventory(User $user)
    {

    }

    public function getById(int $id)
    {
        return $this->userRepository->getById($id);
    }

    /**
     * @param string $date
     * @return string
     */
    public function getStringAuthorisation($date)
    {
        return $this->showDate(strtotime($date));
    }

    public function usersList(int $count = 10, int $start = 0)
    {
        return $this->userRepository->usersList($count, $start);
    }

    /**
     * @param $date
     * @return string
     */
    public function showDate($date)
    {
        $stf      = 0;
        $cur_time = time();
        $diff     = $cur_time - $date;

        $seconds = array( 'секунда', 'секунды', 'секунд' );
        $minutes = array( 'минута', 'минуты', 'минут' );
        $hours   = array( 'час', 'часа', 'часов' );
        $days    = array( 'день', 'дня', 'дней' );
        $weeks   = array( 'неделя', 'недели', 'недель' );
        $months  = array( 'месяц', 'месяца', 'месяцев' );
        $years   = array( 'год', 'года', 'лет' );
        $decades = array( 'десятилетие', 'десятилетия', 'десятилетий' );

        $phrase = array( $seconds, $minutes, $hours, $days, $weeks, $months, $years, $decades );
        $length = array( 1, 60, 3600, 86400, 604800, 2630880, 31570560, 315705600 );

        for ( $i = sizeof( $length ) - 1; ( $i >= 0 ) && ( ( $no = $diff / $length[ $i ] ) <= 1 ); $i -- );
        if ( $i < 0 ) {
            $i = 0;
        }
        $_time = $cur_time - ( $diff % $length[ $i ] );
        $no    = floor( $no );
        $value = sprintf( "%d %s ", $no, $this->getPhrase( $no, $phrase[ $i ] ) );

        if ( ( $stf == 1 ) && ( $i >= 1 ) && ( ( $cur_time - $_time ) > 0 ) ) {
            $value .= time_ago( $_time );
        }

        return $value . ' назад';
    }

    public function getPhrase( $number, $titles ) {
        $cases = array( 2, 0, 1, 1, 1, 2 );

        return $titles[ ( $number % 100 > 4 && $number % 100 < 20 ) ? 2 : $cases[ min( $number % 10, 5 ) ] ];
    }

    public function usersCount()
    {
        return $this->userRepository->count();
    }
}