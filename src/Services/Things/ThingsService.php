<?php
/**
 * Created by PhpStorm.
 * User: muslim
 * Date: 06.11.18
 * Time: 17:33
 */
namespace Services\Things;
use App\Db;
use Interfaces\Things\FoodRepositoryInterface;
use Interfaces\Things\ThingsRepositoryInterface;

class ThingsService
{
    /**
     * @var FoodRepositoryInterface $foodRepository
     */
    private $thingsRepository;

    public function __construct(ThingsRepositoryInterface $thingsRepository)
    {
        $this->thingsRepository = $thingsRepository;
    }

    /**
     * @param integer $id
     */
    public function getThingInfo(int $id)
    {
        $thing = $this->thingsRepository->getById($id);
        $info = (new Db())->getEasyDb()->row('SELECT * FROM test.'.$thing->getClass().' WHERE parent = ?', $thing->getId());

    }
}