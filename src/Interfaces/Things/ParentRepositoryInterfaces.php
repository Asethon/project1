<?php
/**
 * Created by PhpStorm.
 * User: muslim
 * Date: 07.11.18
 * Time: 16:26
 */

namespace Interfaces\Things;

/**
 * Interface ParentInterfaces
 * @package Inventory\Interfaces
 */
interface ParentRepositoryInterfaces
{
    /**
     * ThingsInterface constructor.
     */
    public function __construct();


}