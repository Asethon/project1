<?php
/**
 * Created by PhpStorm.
 * User: muslim
 * Date: 06.11.18
 * Time: 12:34
 */

namespace Interfaces\Things;
use Entities\Things\Things;

/**
 * Interface ThingsInterface
 * @package Things\Interfaces
 */
interface ThingsRepositoryInterface extends ParentRepositoryInterfaces
{
    /**
     * @param int $id
     * @return Things
     */
    public function getById(int $id): Things;

    /**
     * @param string $name
     * @param bool $regex
     * @return Things[]
     */
    public function findByName(string $name, bool $regex): array;

    /**
     * @return Things[]
     */
    public function getAll(): array ;

    /**
     * @param Things $inventory
     */
    public function add(Things $inventory): void ;

    /**
     * @param Things $inventory
     */
    public function update(Things $inventory): void ;

    /**
     * @param Things $inventory
     */
    public function delete(Things $inventory): void ;
}