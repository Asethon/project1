<?php
/**
 * Created by PhpStorm.
 * User: muslim
 * Date: 06.11.18
 * Time: 12:34
 */

namespace Interfaces\Things;
use Entities\Things\Food;

/**
 * Interface FoodInterface
 * @package Inventory\Interfaces
 */
interface FoodRepositoryInterface extends ParentRepositoryInterfaces
{
    /**
     * @param int $id
     * @return Food
     */
    public function getById(int $id): Food;

    /**
     * @param string $name
     * @return Food[]
     */
    public function findByName(string $name): array;

    /**
     * @return Food[]
     */
    public function getAll(): array ;

    /**
     * @param Food $food
     */
    public function add(Food $food): void ;

    /**
     * @param Food $food
     */
    public function update(Food $food): void ;

    /**
     * @param Food $food
     */
    public function delete(Food $food): void ;
}