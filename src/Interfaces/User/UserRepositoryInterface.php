<?php
/**
 * Created by PhpStorm.
 * User: muslim
 * Date: 08.11.18
 * Time: 14:07
 */

namespace Interfaces\User;
use Entities\User\User;
use ParagonIE\EasyDB\EasyDB;

/**
 * Interface UserInterface
 * @package User\Interfaces
 */
interface UserRepositoryInterface
{
    /**
     * UserInterface constructor.
     */
    public function __construct();

    /**
     * @param int $id
     * @return User
     */
    public function getById(int $id);

    /**
     * @param string $name
     * @param bool $regex
     * @return User[]
     */
    public function findByName(string $name, bool $regex = false);

    /**
     * @param int $count
     * @param int $start
     * @return User[]
     */
    public function usersList(int $count = 10, int $start = 0);

    /**
     * @param User $user
     * @return void
     */
    public function update(User $user);

    /**
     * @param string $name
     * @param string $password
     * @param null $email
     * @return User
     */
    public function create(string $name, string $password, $email = null);

    /**
     * @return int
     */
    public function count();
}