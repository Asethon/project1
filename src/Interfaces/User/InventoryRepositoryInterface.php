<?php
/**
 * Created by PhpStorm.
 * User: muslim
 * Date: 14.11.18
 * Time: 17:17
 */

namespace Interfaces\User;
use Entities\User\Inventory;

/**
 * Interface InventoryRepositoryInterface
 * @package Interfaces\User
 */
interface InventoryRepositoryInterface
{
    /**
     * InventoryRepositoryInterface constructor.
     */
    public function __construct();

    /**
     * @param int $id
     * @return Inventory
     */
    public function getById(int $id): Inventory;

    /**
     * @param Inventory $inventory
     * @return void
     */
    public function update(Inventory $inventory): void;

    /**
     * @param int $parentId
     * @param int $userId
     * @return Inventory
     */
    public function create(int $parentId, int $userId): Inventory;

    /**
     * @param int $id
     * @return void
     */
    public function delete(int $id): void ;
}