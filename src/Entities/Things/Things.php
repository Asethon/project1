<?php
namespace Entities\Things;
/**
 * Class Inventory
 * @package Inventory
 */
class Things
{
    /**
     * @var int $id
     */
    private $id;
    /**
     * @var string $name
     */
    protected $name;
    private $description;
    private $class;
    /**
     * @var float $weight
     */
    protected $weight;
    /**
     * @var bool $status
     */
    protected $status;
    /**
     * @var string $img
     */
    protected $img;

    /**
     * Inventory constructor.
     * @param array ...$info
     */
    public function __construct(...$info)
    {
        foreach ($info[0] as $key=>$value) {
            $this->$key = $value;
        }
    }

    /**
     * @return boolean
     */
    public function getStatus(): bool
    {
        return $this->status;
    }
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    /**
     * @return string
     */
    public function getImg(): string
    {
        return '/images/inventory/' . $this->img;
    }
    /**
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }

    /**
     * @param integer $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
    /**
     * @param bool $status
     */
    public function setStatus(bool $status): void
    {
        $this->status = $status;
    }
    /**
     * @param float $weight
     */
    public function setWeight(float $weight): void
    {
        $this->weight = $weight;
    }
    /**
     * @param string $img
     */
    public function setImg(string $img): void
    {
        $this->img = $img;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param mixed $class
     */
    public function setClass($class): void
    {
        $this->class = $class;
    }
}