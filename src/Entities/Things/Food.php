<?php
/**
 * Created by PhpStorm.
 * User: Asethon
 * Date: 03.09.2018
 * Time: 15:24
 */

namespace Entities\Things;

/**
 * Class Food
 * @package Inventory
 */
class Food extends Things
{
    /**
     * @var int $id
     */
    private $id;

    /**
     * @var string $explorationData
     */
    private $explorationData;
    /**
     * @var int $parent
     */
    private $parent;

    /**
     * Food constructor.
     * @noinspection PhpMissingParentConstructorInspection
     * @param mixed ...$info
     */
    public function __construct(...$info)
    {
        foreach ($info[0] as $key=>$value) {
            $this->$key = $value;
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    /**
     * @return mixed
     */
    public function getExplorationDate()
    {
        return $this->explorationData;
    }
    /**
     * @return mixed
     */
    public function getParentId()
    {
        return $this->parent;
    }
}