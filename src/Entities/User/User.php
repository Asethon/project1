<?php
/**
 * Created by PhpStorm.
 * User: muslim
 * Date: 08.11.18
 * Time: 14:10
 */

namespace Entities\User;

/**
 * Class User
 */
class User
{
    /**
     * @var int $id
     */
    private $id;
    /**
     * @var string $name
     */
    private $name;
    /**
     * @var string $password
     */
    private $password;
    /**
     * @var string $email
     */
    private $email;
    /**
     * @var string $img
     */
    private $img;
    private $registrationData;
    private $lastAuthorization;
    /**
     * User constructor.
     * @param array ...$info
     */
    public function __construct(...$info)
    {
        foreach ($info[0] as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return bool
     */
    public function verifyPassword(string $password)
    {
        if ($password == $this->password) return true;

        return false;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getImg(): string
    {
        return '/images/avatars/' . $this->img;
    }

    /**
     * @return string
     */
    public function getRegistrationData()
    {
        return $this->registrationData;
    }

    /**
     * @return string
     */
    public function getLastAuthorization()
    {
        return $this->lastAuthorization;
    }

    /**
     * @param int $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @param string $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @param string $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @param string $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @param string $img
     */
    public function setImg(string $img): void
    {
        $this->img = $img;
    }

    /**
     * @param string $registrationData
     */
    public function setRegistrationData($registrationData): void
    {
        $this->registrationData = $registrationData;
    }

    /**
     * @param string $lastAuthorization
     */
    public function setLastAuthorization($lastAuthorization): void
    {
        $this->lastAuthorization = $lastAuthorization;
    }
}