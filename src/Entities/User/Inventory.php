<?php
/**
 * Created by PhpStorm.
 * User: muslim
 * Date: 14.11.18
 * Time: 17:11
 */

namespace Entities\User;


class Inventory
{
    private $id;
    private $parentId;
    private $userId;

    public function __construct(...$info)
    {
        foreach ($info[0] as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param int $parentId
     */
    public function setParentId($parentId): void
    {
        $this->parentId = $parentId;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId($userId): void
    {
        $this->userId = $userId;
    }
}