<?php
/**
 * Created by PhpStorm.
 * User: muslim
 * Date: 06.11.18
 * Time: 14:52
 */
namespace Repositories\EasyDb\Things;

use Entities\Things\Things;
use Interfaces\Things\ThingsRepositoryInterface;
use App\Db;
use ParagonIE\EasyDB\EasyDB;

/**
 * Class ThingsRepository
 * @package Things\Repository
 */
class ThingsRepository implements ThingsRepositoryInterface
{
    /**
     * @var EasyDB
     */
    private $db;
    /**
     * @var Things[]
     */
    private $things = [];

    /**
     * ThingsRepository constructor.
     */
    public function __construct()
    {
        $this->db = (new Db())->getEasyDb();
    }

    /**
     * @param Things $things
     */
    public function add(Things $things): void
    {
        $img = explode('/',$things->getImg());
        $img = array_pop($img);

        $this->db->insert('things', [
            'id' => $things->getId(),
            'name' => $things->getName(),
            'description' => $things->getDescription(),
            'class' => $things->getClass(),
            'status' => $things->getStatus(),
            'weight' => $things->getWeight(),
            'img' => $img
        ]);
        $id = $this->db->getPdo()->lastInsertId();

        $this->things[$id] = $things;
        // TODO: Implement add() method.
    }

    /**
     * @param Things $things
     */
    public function update(Things $things): void
    {

        $img = explode('/',$things->getImg());
        $img = array_pop($img);
        $this->db->update('things', [
            'name' => $things->getName(),
            'status' => $things->getStatus(),
            'weight' => $things->getWeight(),
            'img' => $img
        ],
            [
                'id' => $things->getId()
            ]);
    }

    /**
     * @param Things $things
     */
    public function delete(Things $things): void
    {
        $this->db->delete('things', ['id' => $things->getId()]);
        unset($this->things[$things->getId()]);
    }

    /**
     * @return Things[]
     */
    public function getAll(): array
    {
        $info = $this->db->run('SELECT id FROM test.things');

        foreach ($info as $value)
        {
            $this->getById($value['id']);
        }
        return $this->things;
        // TODO: Implement getAll() method.
    }

    /**
     * @param int $id
     * @return Things
     */
    public function getById(int $id): Things
    {
        /**
         * @var array $info
         */
        if (empty($this->things[$id])) {
            $info = $this->db->row('SELECT * FROM test.things WHERE id = ?', $id);
            $this->things[$id] = new Things($info);
        }

        return $this->things[$id];
        // TODO: Implement getById() method.
    }

    /**
     * @param string $name
     * @param bool $regex
     * @return Things[]
     * @throws \Exception
     */
    public function findByName(string $name, bool $regex): array
    {
        $search = '^' . $name . '.*';
        $info = $this->db->run("SELECT id FROM test.things WHERE name REGEXP ?", $search);
        $things = [];
        foreach ($info as $key=>$value)
        {
            $things[$key] = $this->getById($value['id']);
        }

        if (count($things) == 0) throw new \Exception('Not found');

        return $things;
        // TODO: Implement searchByName() method.
    }

    public function count()
    {
        return count($this->things);
    }
}