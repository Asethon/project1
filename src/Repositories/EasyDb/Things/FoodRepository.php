<?php
/**
 * Created by PhpStorm.
 * User: muslim
 * Date: 06.11.18
 * Time: 14:52
 */

namespace Repositories\EasyDb\Things;

use App\Db;
use Entities\Things\Food;
use Interfaces\Things\FoodRepositoryInterface;

use ParagonIE\EasyDB\EasyDB;

class FoodRepository implements FoodRepositoryInterface
{
    /**
     * @var EasyDB
     */
    private $db;

    /**
     * @var Food[]
     */
    private $food = [];

    /**
     * ThingsRepository constructor.
     */
    public function __construct()
    {
        $this->db = (new Db())->getEasyDb();
    }

    public function add(Food $food): void
    {
        // TODO: Implement add() method.
        $this->db->insert("food", [
            $food->getName(),
            $food->getStatus(),
            $food->getWeight(),
            $food->getImg()
        ]);
        $id = $this->db->getPdo()->lastInsertId();

        $this->food[$id] = $food;
    }

    public function update(Food $food): void
    {
        $this->db->update('food', [
            'parent' => $food->getParentId(),
            'status' => $food->getExplorationDate(),
        ],
            [
                'id' => $food->getId()
            ]);
    }

    /**
     * @param Food $food
     */
    public function delete(Food $food): void
    {
        $this->db->delete('inventory', ['id' => $food->getId()]);
        unset($this->food[$food->getId()]);
    }

    /**
     * @return Food[]
     */
    public function getAll(): array
    {
        $info = $this->db->run('SELECT id FROM test.food');

        foreach ($info as $value)
        {
            $this->getById($value['id']);
        }
        return $this->food;
        // TODO: Implement getAll() method.
    }

    /**
     * @param int $id
     * @return Food
     */
    public function getById(int $id): Food
    {
        if (empty($this->food[$id])) {
            $info = $this->db->row('SELECT * FROM test.food WHERE id = ?', $id);

            $this->food[$id] = new Food($info);
        }
        return $this->food[$id];
        // TODO: Implement getById() method.
    }

    /**
     * @param string $name
     * @return array
     */
    public function findByName(string $name): array
    {
        // TODO: Implement searchByName() method.
    }
}