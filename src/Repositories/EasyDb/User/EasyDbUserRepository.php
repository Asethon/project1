<?php
/**
 * Created by PhpStorm.
 * User: muslim
 * Date: 08.11.18
 * Time: 14:26
 */

namespace Repositories\EasyDb\User;

use App\Db;
use Interfaces\User\UserRepositoryInterface;
use Entities\User\User;


class EasyDbUserRepository implements UserRepositoryInterface
{
    private $db;
    private $users = [];

    /**
     * EasyDbUserRepository constructor.
     */
    public function __construct()
    {
        $this->db = (new Db())->getEasyDb();
    }

    public function update(User $user)
    {
        $this->db->update('users', [
            'name' => $user->getName(),
            'password' => $user->getPassword(),
            'email' => $user->getEmail(),
            'registrationData' => $user->getRegistrationData(),
            'lastAuthorization' => $user->getLastAuthorization()
        ],
            [
                'id' => $user->getId()
            ]);
    }

    /**
     * @param int $id
     * @return User
     */
    public function getById(int $id)
    {
        /**
         * @var array $info
         */
        if (empty($this->users[$id])) {
            $info = $this->db->row('SELECT * FROM test.users WHERE id = ?', $id);
            $this->users[$id] = new User($info);
        }
        return $this->users[$id];
        // TODO: Implement getById() method.
    }

    /**
     * @param string $name
     * @param bool|null $regex
     * @return User[]
     * @throws \Exception
     */
    public function findByName(string $name, bool $regex = false): array
    {
        if (!$regex) {
            $info = $this->db->run('SELECT id FROM test.users WHERE BINARY name = ?', $name);
        } else {
            $search = '^' . $name . '.*';
            $info = $this->db->run("SELECT id FROM test.users WHERE name REGEXP ?", $search);
        }
        $users = [];
        foreach ($info as $key => $value) {
            if (empty($this->users[$value['id']])) {
                $users[$key] = $this->getById($value['id']);
            } else {
                $users[$key] = $this->users[$value['id']];
            }
        }

        if (count($users) == 0) throw new \Exception('User not found');

        return $users;
    }

    /**
     * @param int $count
     * @param int $start
     * @return User[]
     * @throws \Exception
     */
    public function usersList(int $count = 10, int $start = 1)
    {
        $info = $this->db->run("SELECT id FROM test.users LIMIT $count OFFSET $start");
        $users = [];
        foreach ($info as $key => $value) {
            if (empty($this->users[$value['id']])) {
                $users[$key] = $this->getById($value['id']);
            } else {
                $users[$key] = $this->users[$value['id']];
            }
        }

        if (count($users) == 0) throw new \Exception('User not found');

        return $users;
    }


    /**
     * @param string $name
     * @param string $password
     * @param null $email
     * @return User
     * @throws \Exception
     */
    public function create(string $name, string $password, $email = null)
    {
        try {
            $this->findByName($name);
        } catch (\Exception $e) {
            $id = $this->db->row('SELECT MAX(id) FROM users');
            $id = $id['MAX(id)'] + 1;
            $info = ['id' => $id, 'name' => $name, 'password' => $password, 'email' => $email];
            $user = new User($info);

            $this->db->insert('users', [
                'id' => $user->getId(),
                'name' => $user->getName(),
                'password' => $user->getPassword(),
                'email' => $user->getEmail(),
                'registrationData' => date('Y-m-d H:i:s'),
                'lastAuthorization' => date('Y-m-d H:i:s')
            ]);

            $this->users[$id] = $user;
            return $this->users[$id];
        }
        echo $this->count();
        throw new \Exception('Данный логин уже используется');
    }

    public function count()
    {
        $count = $this->db->row('SELECT COUNT(*) FROM users');
        return $count['COUNT(*)'];
    }
}