<?php
/**
 * Created by PhpStorm.
 * User: muslim
 * Date: 14.11.18
 * Time: 17:15
 */

namespace Repositories\EasyDb\User;

use App\Db;
use Entities\User\Inventory;
use Interfaces\User\InventoryRepositoryInterface;
use ParagonIE\EasyDB\EasyDB;

class InventoryRepository implements InventoryRepositoryInterface
{
    /**
     * @var EasyDB
     */
    private $db;

    /**
     * @var Inventory[]
     */
    private $inventory = [];
    /**
     * InventoryRepository constructor.
     */
    public function __construct()
    {
        $this->db = (new Db())->getEasyDb();
    }

    /**
     * @param int $id
     * @return Inventory
     */
    public function getById(int $id): Inventory
    {
        /**
         * @var array $info
         */
        if (empty($this->inventory[$id])) {
            $info = $this->db->row('SELECT * FROM test.inventory WHERE id = ?', $id);
            $this->inventory[$id] = new Inventory($info);
        }
        return $this->inventory[$id];
        // TODO: Implement getById() method.
    }

    /**
     * @param Inventory $inventory
     * @return void
     */
    public function update(Inventory $inventory): void
    {
        $this->db->update('users', [
            'parentId' => $inventory->getParentId(),
            'userId' => $inventory->getUserId(),
        ],
            [
                'id' => $inventory->getId()
            ]);
        // TODO: Implement update() method.
    }

    /**
     * @param int $parentId
     * @param int $userId
     * @return Inventory
     */
    public function create(int $parentId, int $userId): Inventory
    {
        $id = $this->db->row('SELECT MAX(id) FROM test.inventory');
        $id = $id['MAX(id)'] + 1;
        $info = ['id' => $id, 'parentId' => $parentId, 'userId' => $userId];
        $inventory = new Inventory($info);

        $this->db->insert('users', [
            'id' => $inventory->getId(),
            'parentId' => $inventory->getParentId(),
            'userId' => $inventory->getUserId(),
        ]);

        $this->inventory[$id] = $inventory;
        return $this->inventory[$id];
        // TODO: Implement create() method.
    }

    /**
     * @param int $id
     * @return void
     */
    public function delete(int $id): void
    {
        // TODO: Implement delete() method.
    }
}