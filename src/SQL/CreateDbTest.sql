CREATE DATABASE test CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE test.things (
  id INT(12) NOT NULL AUTO_INCREMENT,
  name VARCHAR(40) NOT NULL,
  description VARCHAR(120),
  class VARCHAR(40) NOT NULL,
  status BOOLEAN NOT NULL,
  weight FLOAT(12),
  img VARCHAR(40),
  PRIMARY KEY (id)
);

CREATE TABLE test.food (
  id INT(12) NOT NULL AUTO_INCREMENT,
  parent INT(12) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (parent) REFERENCES test.things(id),
  explorationData DATETIME NOT NULL
);

CREATE TABLE test.users (
  id INT(12) NOT NULL AUTO_INCREMENT,
  name VARCHAR(15) NOT NULL,
  password VARCHAR(40) NOT NULL ,
  email VARCHAR(20),
  registrationData DATETIME NOT NULL,
  lastAuthorization DATETIME NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE test.inventory (
  id INT(12) NOT NULL AUTO_INCREMENT,
  parentId INT(12) NOT NULL,
  userId INT(12) NOT NULL,
  FOREIGN KEY (parentId) REFERENCES test.things(id),
  FOREIGN KEY (userId) REFERENCES test.users(id),
  PRIMARY KEY (id)
);