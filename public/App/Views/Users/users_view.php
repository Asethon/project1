<?php
/**
 * Created by PhpStorm.
 * User: muslim
 * Date: 17.01.19
 * Time: 17:33
 */
/**
 * @var array $detail
 */

/** @var Entities\User\User[] $users
 * @var integer $count
 * @var array $lastAuthorization
 */

foreach ($users as $id => $user) {
    ?>
    <div class="block list">
        <div class="image__section list">
            <img src="<?= $user->getImg() ?>"/>
            <div class="user_info">
                <p><b><?= $user->getId() ?></b>.<b><?= $user->getName() ?></b></p>
                <p><?= $lastAuthorization[$user->getId()]?></p>
                <a href="<?= $detail[$user->getId()]?>">Подробнее</a>
            </div>
        </div>
    </div>
    <?
}
?>
<div class="debug">
    Количество пользователей: <?= $count?>
</div>
