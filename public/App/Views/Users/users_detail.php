<?php
/**
 * Created by PhpStorm.
 * User: muslim
 * Date: 17.01.19
 * Time: 17:58
 */

/**
 * @var array $data
 */

/**
 * @var Entities\User\User $user
 */

?>
<div class="block_detail">
    <div class="image__section">
        <img src="<?=$user->getImg()?>"/>
        <div class="user_info">
            <p><b>Идентификатор:</b> <?= $user->getId()?></p>
            <p><b>Имя персонажа:</b> <?= $user->getName()?></p>
            <p><b>Дата регистрации:</b> <?=strftime("%d %B %G", strtotime($user->getRegistrationData()))?></p>
            <p><b>Последняя авторизация:</b> <?= $lastAuthorization?></p>
            <a href="<?= $back_url?>">Назад</a>
        </div>
    </div>
</div>
<?