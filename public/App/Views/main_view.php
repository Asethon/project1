<?php
/**
 * Created by PhpStorm.
 * User: muslim
 * Date: 28.11.18
 * Time: 13:39
 */

use Repositories\EasyDb\Things\{FoodRepository, ThingsRepository};
use Repositories\EasyDb\User\EasyDbUserRepository;
use Services\User\UserService;

$userRepository = new EasyDbUserRepository();
$service = new UserService($userRepository);
$thingsRepository = new ThingsRepository();
$foodRepository = new FoodRepository();

try {
    if (isset($_COOKIE['user']) && isset($_COOKIE['password']) && isset($_COOKIE['id'])) {
        $user = $userRepository->getById($_COOKIE['id']);

        if ($_COOKIE['user'] == md5($user->getName()) && $_COOKIE['password'] == md5($user->getPassword())) {
            echo 'Уже авторизован';
        } else {
            echo 'Неверный логин или пароль';
        }
    } else {
        $user = $userRepository->getById($service->login('User', 'test')->getId());
        echo 'Авторизация';
    }


    $date = DateTime::createFromFormat("Y-m-d H:i:s", $user->getLastAuthorization());

    $diff = (new DateTime())->diff($date);
    $text = '';
    if ($diff->d > 0) $text .= $diff->d . ' дней ';
    if ($diff->h > 0) $text .= $diff->h . ' часов';
    if ($diff->i > 0) $text .= $diff->i . ' секунд';
    if ($text != '') {
        $text .= ' назад';
    } else {
        $text = 'сейчас';
    }
    ?>
    <div class="items">
        <p><b>Username:</b> <?= $user->getName() ?></p>
        <p><b>Email:</b> <?= $user->getEmail() ?></p>
        <p><b>Последний раз заходил:</b> <?= $text ?></p>
    </div>
    <?
} catch (Exception $e) {
    ?>
    <pre>
        <?
        print_r($e) ?>
    </pre>
    <?
}

$food = $foodRepository->getById(1);
$info = $thingsRepository->getById($food->getParentId());

$allFood = $foodRepository->getAll();

if ($allFood) {
    foreach ($allFood as $value) {
        $parent = $thingsRepository->getById($value->getParentId());
        ?>
        <div class="items">
            <p><b>ID:</b> <?= $parent->getId() ?></p>
            <p><b>Name:</b> <?= $parent->getName() ?></p>
            <p><b>Weight:</b> <?= $parent->getWeight() ?></p>
            <img src="<?= $parent->getImg() ?>"/>
        </div>
        <div class="items">
            <p><b>Exploration data:</b> <?= $value->getExplorationDate() ?></p>
        </div>
        <?
    }
}
//$info->setName('NewObject');
//$inventoryRepository->add($info);
//$inventoryRepository->update($info);
?>

