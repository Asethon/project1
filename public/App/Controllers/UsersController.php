<?php
/**
 * Created by PhpStorm.
 * User: muslim
 * Date: 21.12.18
 * Time: 15:50
 */

use Repositories\EasyDb\User\EasyDbUserRepository;
use Services\User\UserService;

class UsersController extends Controller
{
    private $template_dir = 'Users/';

    function action_index()
    {
        $userService = new UserService(new EasyDbUserRepository());

        /** @var \Entities\User\User $user */
        isset($_REQUEST['page']) ? $start = ($_REQUEST['page'] - 1) * 10 : $start = 0;
        try {
            $users = $userService->usersList(10, $start);
        } catch (Exception $exception) {
            header('Location: /404');
            die();
        }

        $usersCount = $userService->usersCount();
        foreach ($users as $user) {
            $detail[$user->getId()] = "/users/detail/" . $user->getId() . "/back=" . $_REQUEST['page'];
            /**
             * @var \Entities\User\User $user
             */
            $lastAuthorisation[$user->getId()] =
                $userService->getStringAuthorisation($user->getLastAuthorization());
        }

        if (empty($lastAuthorisation)) $lastAuthorisation = ['Никогда'];

        $data = ['users' => $users, 'count' => $usersCount, 'lastAuthorization' => $lastAuthorisation];
        if (isset($detail)) $data['detail'] = $detail;
        $this->view->generate($this->template_dir . 'users_view.php', 'template_view.php', $data);
    }

    function action_detail()
    {
        $userService = new UserService(new EasyDbUserRepository());
        $user = $userService->getById($_REQUEST['id']);
        $data['user'] = $user;
        $data['lastAuthorization'] = $userService->getStringAuthorisation($data['user']->getLastAuthorization());
        isset($_REQUEST['list_page']) && $_REQUEST['list_page'] != 1 ? $data['back_url'] = '/users/' . $_REQUEST['list_page'] : $data['back_url'] = '/users';
        $this->view->generate($this->template_dir . 'users_detail.php', 'template_view.php', $data);
    }
}