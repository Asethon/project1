<?php

use Symfony\Component\Routing\{
    Matcher\UrlMatcher,
    RequestContext,
    Loader\YamlFileLoader
};
use Symfony\Component\Config\FileLocator;

/**
 * Created by PhpStorm.
 * User: muslim
 * Date: 27.11.18
 * Time: 18:06
 */
class Route
{
    /**
     * @throws Exception
     */
    static function start()
    {
        $fileLocator = new FileLocator(array(__DIR__ . '/../'));
        $loader = new YamlFileLoader($fileLocator);
        $routes = $loader->load('routes.yaml');
        $context = new RequestContext('');
        $matcher = new UrlMatcher($routes, $context);
        try {
            $parameters = $matcher->match($_SERVER['REQUEST_URI']);
            foreach ($parameters as $key=>$param)
            {
                $_REQUEST[$key] = $param;
            }
            /*foreach ($parameters as $key=>$param) {
                if (substr($key, 0, 1) == '_') continue;
                $back = $routes->get($_REQUEST['_route']);
                $url = str_replace('{' . $key . '}', $param, $back->getPath());
            }*/
            $explode = explode('::', $parameters['_controller']);
        } catch (Exception $exception) {
            header('Location: /404');
            die();
        }

        $controller_name = $explode[0];
        $action_name = $explode[1];

        $model_name = 'Model' . ucfirst($controller_name);

        $model_file = strtolower($model_name) . '.php';
        $model_path = "App/Models/" . $model_file;
        if (file_exists($model_path)) {
            include "App/Models/" . $model_file;
        }

        $controller_path = "App/Controllers/" . $controller_name . '.php';

        if (file_exists($controller_path)) {
            include $controller_path;
        } else {
            Route::ErrorPage404();
            $controller_name = "NotFoundController";
            include "App/Controllers/". $controller_name . ".php";
        }

        $controller = new $controller_name;
        $action = $action_name;
        if (method_exists($controller, $action)) {
            $controller->$action();
        } else {
            Route::ErrorPage404();
        }
    }

    static function ErrorPage404()
    {
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:' . $host . '404');
    }
}