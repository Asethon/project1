<?php
/**
 * Created by PhpStorm.
 * User: muslim
 * Date: 28.11.18
 * Time: 13:31
 */

class View
{
    function generate($content_view, $template_view, $data = null)
    {
        if(is_array($data)) {
            extract($data);
        }

        include 'App/Views/'.$template_view;
    }
}