<?php
/**
 * Created by PhpStorm.
 * User: muslim
 * Date: 13.11.18
 * Time: 13:50
 */

namespace App;
use ParagonIE\EasyDB\Factory;

class Db
{
    public $easyDb;
    private $ROOT_PATH = '../core';

    public function __construct()
    {

        $settings = $this->getPDOSettings();
        $this->easyDb = Factory::create($settings['dsn'], $settings['user'], $settings['pass']);
    }

    /**
     * @return \ParagonIE\EasyDB\EasyDB
     */
    public function getEasyDb(): \ParagonIE\EasyDB\EasyDB
    {
        return $this->easyDb;
    }

    protected function getPDOSettings()
    {
        $config = include $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $this->ROOT_PATH.DIRECTORY_SEPARATOR.'Config'.DIRECTORY_SEPARATOR.'db.php';
        $result['dsn'] = "{$config['type']}:host={$config['host']};dbname={$config['dbname']};}";
        $result['user'] = $config['user'];
        $result['pass'] = $config['pass'];
        return $result;
    }
}